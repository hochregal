#include <sys/types.h>
#include <sys/socket.h>
#include <stdio.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <pthread.h>
#include <unistd.h>
#include <stdbool.h>





/***************SERVER****************/

void *Parser(void*);
void *Receive(void *);
void *Send(void *);
void *Timer_X(void *);
void *Timer_Y(void *);
int CreateSocket(int);

pthread_mutex_t simu_mutex;
pthread_cond_t sensor_trigger;

#pragma pack (1)
struct commands
{
  unsigned int xon  :1;
  unsigned int xdir :1;
  unsigned int yon  :1;
  unsigned int ydir :1;
  unsigned int zon  :1;
  unsigned int zdir :1;
  unsigned int grep :1;
};
#pragma pack()



#pragma pack (2)
struct sensors
{
  unsigned int x :4;
  unsigned int y :4;
  unsigned int z :2;
};
#pragma pack()

struct commands my_commands;
struct sensors my_sensors;


//pthread_attr_t attr;

int main(int argc, char *arcv[])
{
  pthread_t parser, send, receive;
  int server_socket, new_socket, rc;

  my_sensors.x=1;
  my_sensors.y=1;
  my_sensors.z=1;

  pthread_mutex_init(&simu_mutex, NULL);
  pthread_cond_init (&sensor_trigger, NULL);

  new_socket = CreateSocket(server_socket);

  //  pthread_attr_init(&attr);
  //  pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);


  rc = pthread_create(&receive, NULL, Receive, (void *) new_socket);
  if (rc){
    printf("ERROR; return code from pthread_create() receive is %d\n", rc);
  }

  rc = pthread_create(&send, NULL, Send, (void *) new_socket);
  if (rc){
    printf("ERROR; return code from pthread_create() send is %d\n", rc);
  }
  /*
  rc = pthread_create(&parser, &attr, Parser, NULL);
  if (rc){
    printf("ERROR; return code from pthread_create() parser is %d\n", rc);
  }
  */

  /*
  rc = pthread_create(&timer_y, &attr, Timer_Y, NULL);
  if (rc){
    printf("ERROR; return code from pthread_create() timer_y is %d\n", rc);
  }
  */
  //pthread_join(send, NULL);
  //pthread_join(receive, NULL);

  //pthread_join(parser, NULL);

  //pthread_join(timer_y, NULL);

  while(1)
    ;


  
  printf("\n Server: close()...");

  close(server_socket);
  close(new_socket);

  printf("\n Serverprogramm beendet\n\n");


  //  pthread_attr_destroy(&attr);
  pthread_mutex_destroy(&simu_mutex);
  pthread_cond_destroy(&sensor_trigger);
  pthread_exit(NULL);
  return(0);
}

int CreateSocket(int server_socket)
{
  int new_socket;
  int length;
  struct sockaddr_in serverinfo, clientinfo;
  unsigned short int portnmbr=5000;
  char ip_addr[]="127.0.0.1";

  printf("\n Server: socket()...");

  server_socket = socket(AF_INET, SOCK_STREAM, 0);
  serverinfo.sin_family = AF_INET;
  serverinfo.sin_addr.s_addr = inet_addr(ip_addr);
  serverinfo.sin_port = htons(portnmbr);
  length = sizeof(serverinfo);

  printf("\n Server: bind()...");
  bind(server_socket, (struct sockaddr *)&serverinfo, length);
  printf("\n Server: listen()...");
  printf("\n Server mit IP %s", ip_addr);
  printf(" an Port %d wartet...", portnmbr);

  listen(server_socket, 3);
  printf("\n Server: accept()...");
  new_socket = accept(server_socket,
		      (struct sockaddr *)&clientinfo, &length);

  printf("Verbindung mit %s", inet_ntoa(clientinfo.sin_addr));

  return new_socket;
}

void *Parser(void* arg)
{
  char input[10];
  printf("\n<Parser> started");
  while(1)
    {
      printf("\n~>");
      scanf("%10s", input);
      fflush(stdin);
      if(strstr(input, "test"))
	  printf("\nTesteingabe");

    }
  pthread_exit(NULL);
}

void *Receive(void *new_socket)
{
  pthread_t timer_x, timer_y;
  int socket, rc;
  socket=(int) new_socket;

  printf("\n<Receive> started");

  while(1)
    {
      int test;
      //pthread_mutex_lock(&simu_mutex);
      rc=recv(socket, (struct commands *)&my_commands, sizeof(my_commands),0);

      printf("\n%d Bytes empfangen <new commands>", rc);

      if(my_commands.xon)
	{
	  printf("\nxon");
	  fflush(stdout);
	  rc = pthread_create(&timer_x, NULL, Timer_X, NULL);
	  if (rc)
	    printf("\nERROR; return code from pthread_create() timer_x is %d\n", rc);
	  //pthread_join(timer_x, NULL);
	}
      //      my_commands.xon=1;
      printf("\nxon %d", my_commands.xon);
      printf(" xdir %d", my_commands.xdir);
      printf(" yon %d", my_commands.yon);
      printf(" ydir %d", my_commands.ydir);
      printf(" zon %d", my_commands.zon);
      printf(" zdir %d", my_commands.zdir);
      printf(" grep %d", my_commands.grep);

      fflush(stdout);
      //pthread_mutex_unlock(&simu_mutex);

    }
  pthread_exit(NULL);
}

void *Send(void *new_socket)
{
  int socket;
  socket=(int) new_socket;
  int len;

  printf("\n<Send> started");

  while(1)
    {
      pthread_mutex_lock(&simu_mutex);
      pthread_cond_wait(&sensor_trigger, &simu_mutex);
      printf("\n<Send> received Signal");
      len=send(socket, (struct sensors*)&my_sensors, sizeof(my_sensors),0);
      printf("\nsent %d bytes",len);
      fflush(stdout);
      pthread_mutex_unlock(&simu_mutex);  
    }
  pthread_exit(NULL);

}

void *Timer_X(void * arg)
{
  int i, sensor;

  printf("\n<TimerX> started");
  fflush(stdout);
  while(1)
    {
      usleep(1000000);
      if( my_sensors.x>0 && my_sensors.x<11)
	{
	  if(my_commands.xdir)
	    my_sensors.x++;
	  else
	    my_sensors.x--;
	}
      else
	printf("[ERROR] *****MACHINE OUT OF X-RANGE!!!*****");
      
      pthread_mutex_lock(&simu_mutex);
      pthread_cond_signal(&sensor_trigger);
      pthread_mutex_unlock(&simu_mutex);
    }
  pthread_exit(NULL);
}

void *Timer_Y(void *arg)
{
  int i;

  printf("\n<TimerY> started");
  /*
    while(1)
    {
    usleep(1000000);
    if( my_sensors.y>0 && my_sensors.y<11)
    {
    if(my_commands.ydir)
    my_sensors.y++;
    else
    my_sensors.y--;
    }
    else
    printf("[ERROR] *****MACHINE OUT OF X-RANGE!!!*****");
    
    pthread_mutex_lock(&simu_mutex);
    pthread_cond_signal(&sensor_trigger);
    pthread_mutex_unlock(&simu_mutex);
    }
  */
  pthread_exit(NULL);
}
