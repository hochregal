#include <sys/types.h>
#include <sys/socket.h>
#include <stdio.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <pthread.h>
#include <string.h>

#pragma pack (1)
struct commands
{
  unsigned int xon  :1;
  unsigned int xdir :1;
  unsigned int yon  :1;
  unsigned int ydir :1;
  unsigned int zon  :1;
  unsigned int zdir :1;
  unsigned int grep :1;
};
#pragma pack()


#pragma pack (2)
struct sensors
{
  unsigned int x :4;
  unsigned int y :4;
  unsigned int z :2;
};
#pragma pack()

struct commands my_commands;
struct sensors my_sensors;

/***************CLIENT****************/

void *Receive(void *my_socket)
{
  int rc, socket=(int) my_socket;
  char rec[sizeof(my_sensors)];

  printf("\n<Receive> started");
  
  while(1)
    {
      rc=recv(socket, (struct sensors *)&my_sensors, sizeof(my_sensors),0);
      printf("\n%d Bytes empfangen", rc);
      printf("sensors x: %d", (int)my_sensors.x);
    }
  pthread_exit(NULL);
}

void *Parser(void *my_socket)
{
  int socket=(int) my_socket;
  char input[18];
  int len;
  int test=23;

  printf("\n<Parser> started");
  printf("\n~>");
  scanf("%18s", input);

  fflush(stdin);

  if(strstr(input, "startx"))  
    {
      my_commands.xon=1;
      printf("\nxon %d", my_commands.xon);

      if(strstr(input, "forward"))
	{
	  printf("forward");
	  my_commands.xdir=1;
	}
      else
	{
	  printf("backward");
	  my_commands.xdir=0;
	}
      printf("\nxon %d", my_commands.xon);
      
    }
  else
    {
      printf("no");
      my_commands.xon=0;
    }

  printf("\nxon %d", my_commands.xon);
  printf(" xdir %d", my_commands.xdir);
  printf(" yon %d", my_commands.yon);
  printf(" ydir %d", my_commands.ydir);
  printf(" zon %d", my_commands.zon);
  printf(" zdir %d", my_commands.zdir);
  printf(" grep %d", my_commands.grep);

  len=send(socket, (struct commands *)&my_commands, sizeof(my_commands),0);
  printf("\nsent %d bytes",len);
  fflush(stdout);
  pthread_exit(NULL);

}

int main(int argc, char *arcv[])
{
  int socket_nmbr;
  int length, rc;
  struct sockaddr_in addresinfo;
  unsigned short int portnmbr = 5000;
  char ip_addres[] = "127.0.0.1";

  pthread_t parser, receive;

  /*
  my_commands.xon  = 0;
  my_commands.xdir = 0;
  my_commands.yon  = 0;
  my_commands.ydir = 0;
  my_commands.zon  = 0;
  my_commands.zdir = 0;
  my_commands.grep = 0;
  */
  printf("\n Client gestartet");

  socket_nmbr = socket(AF_INET, SOCK_STREAM, 0);
  addresinfo.sin_family = AF_INET;
  addresinfo.sin_addr.s_addr = inet_addr(ip_addres);
  addresinfo.sin_port = htons(portnmbr);
  length = sizeof(addresinfo);

  if (!connect(socket_nmbr, (struct sockaddr *)&addresinfo, length))
    {
      printf("\n Client: Verbindungsaufbau erfolgreich an");
      printf(" IP %s - Port %d", ip_addres, portnmbr);
    }

  rc = pthread_create(&parser, NULL, Parser, (void *) socket_nmbr);
  if (rc){
    printf("ERROR; return code from pthread_create() send is %d\n", rc);
  }


  rc = pthread_create(&receive, NULL, Receive, (void *) socket_nmbr);
  if (rc){
    printf("ERROR; return code from pthread_create() send is %d\n", rc);
  }

  while(1)
    ;

  close(socket_nmbr);
  printf("\n Client beendet");
  pthread_exit(NULL);
  return 0;
}
